const express = require('express');
const dotenv = require('dotenv')
dotenv.config()
var cors = require('cors')
const bodyParser = require('body-parser')
const cluster = require('cluster')
const compression = require('compression')
const helmet = require('helmet')


// Set up mongoose connection
const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/logique', { useNewUrlParser: true});
mongoose.Promise = global.Promise;
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));





if (cluster.isMaster) {
  var numWorkers = require('os').cpus().length;

  console.log('Master cluster setting up ' + numWorkers + ' workers...')

  for (var i = 0; i < numWorkers; i++) {
    cluster.fork()
  }

  cluster.on('online', function (worker) {
    console.log('Worker ' + worker.process.pid + ' is online')
  })

  cluster.on('exit', function (worker, code, signal) {
    console.log('Worker ' + worker.process.pid + ' died with code: ' + code + ', and signal: ' + signal)
    console.log('Starting a new worker')
    cluster.fork()
  })
} else {
  var app = require('express')()
  //App Use
  app.disable('etag');
  // enabling compression for faster process
  app.use(compression())
  // enabling CORS for all requests
  app.use(cors())
  // adding Helmet to enhance your API's security
  app.use(helmet())
  app.use(express.json({
    limit: '50mb'
  }));
  app.use(express.urlencoded({
    limit: '50mb'
  }));
  // parse application/json
  app.use(bodyParser.json())

  // parse application/x-www-form-urlencoded
  app.use(bodyParser.urlencoded({
    extended: true
  }))
  
  //Load Routes
  require('./app/routes')(app)
  //Load Routes
  const port = process.env.PORT

  var server = app.listen(port, function () {
    console.log('Process ' + process.pid + ' is listening to all incoming requests')
  })
  module.exports = server //for testing
}