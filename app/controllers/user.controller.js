
const User = require('../models/user.model')
const creditcardHelper = require('../helpers/creditcard')
var crypto = require('crypto')
const {
  Validator
} = require('node-input-validator')
//register
exports.register = function (req, res) {
    const validation = new Validator(req.body, {
      name: 'required',
      address:'required',
      email: 'required|email',
      password:'required',
      creditcard_type: 'required',
      creditcard_number: 'required',
      creditcard_expired: 'required',
      creditcard_cvv: 'required',
    })
    try {
      //Checking Required Fields
      validation.check().then(async function (matched) {
        if (matched) {
          //check if email is already registered
        var { name, email, address, password, creditcard_type,creditcard_number, creditcard_expired, creditcard_cvv } = req.body
        email = email.toString().toLowerCase()
          User.findOne({
            email: email
          }, function (err, user) {
            if (err) {
              console.log(err)
              var ret = {
                error: 'Something went wrong. Please try again later.'
              }
              return res.type('json').status(500).send(JSON.stringify(ret))
            } else if (user) {
                var ret = {
                  error: 'User is already registered'
                }
                return res.type('json').status(500).send(JSON.stringify(ret))
            } else {
                var photos = []
                var files = req.files
                files.map((f, i) => {
                  url = f.fieldname != undefined ? ('/uploads/' + f.filename) : ''
                  photos.push(url)
                })
                if(photos.length==0){
                    var ret = {
                      error: {"error":"Please provide photos fields."}
                    }
                    return res.type('json').status(500).send(JSON.stringify(ret))
                }
                //credit card type validation
                if(creditcardHelper.getCardType(creditcard_number)!=creditcard_type){
                    var ret = {
                      error: {"error":"Credit card data invalid"}
                    }
                    return res.type('json').status(400).send(JSON.stringify(ret))
                }
                //credit card number validation
                if(creditcardHelper.validateCardNumber(creditcard_number)){
                    var ret = {
                      error: {"error":"Credit card data invalid"}
                    }
                    return res.type('json').status(400).send(JSON.stringify(ret))
                }
                  var dataCreate = {
                    name: name,
                    address: address,
                    email: email,
                    photos:photos,
                    password: crypto.createHash('md5').update(password).digest('hex'),
                    creditcard_type: creditcard_type,
                    creditcard_number: creditcard_number,
                    creditcard_expired: creditcard_expired,
                    creditcard_cvv: creditcard_cvv,
                    createdDate: Date.now()
                  }
                  User.create(dataCreate, function (err, user) {
                    if (err || user == null) {
                        var ret = {
                          error: 'Something went wrong. Please try again later.'
                        }
                        return res.type('json').status(500).send(JSON.stringify(ret))
                    } else {
                        var ret = {
                            user:user._id
                        }
                        return res.type('json').status(200).send(JSON.stringify(ret))
                    }
              })
            }
          })
        } else {
            var ret = {
              error: {"error":"Please provide "+validation.errors+" fields."}
            }
            return res.type('json').status(500).send(JSON.stringify(ret))
        }
      })
    } catch (e) {
      console.log(e)
      var ret = {
        error: 'Something went wrong. Please try again later.'
      }
      return res.type('json').status(500).send(JSON.stringify(ret))
    }
}

//list
exports.list = function (req, res) {
    try {
        var q = req.header('q')
        const ob = req.header('ob')
        const sb = req.header('sb')
        const of = req.header('of')
        const lt = req.header('lf')
        var select = {}
        var sort = {}
        if(typeof q!=="undefined" || q!=null) {
            q=q.toString().split(",")
            q.map(o=>{
                select[o] = 1
            })
            console.log(select)
        }
        if(typeof ob!="undefined" && typeof sb!="undefined") {
            sort = {[ob]:sb=='asc'?1:-1}
        }

        console.log("select",select)
          User.find({}).select(select).sort(sort).skip(of).limit(lt).exec(function(err, users){
            if (err) {
              console.log(err)
              var ret = {
                error: 'Something went wrong. Please try again later.'
              }
              return res.type('json').status(500).send(JSON.stringify(ret))
            } else {
                var ret = {
                    count: users.length,
                    rows:users
                }
                return res.type('json').status(200).send(JSON.stringify(ret))
        }
      })
    } catch (e) {
      console.log(e)
      var ret = {
        error: 'Something went wrong. Please try again later.'
      }
      return res.type('json').status(500).send(JSON.stringify(ret))
    }
}

//get
exports.get = function (req, res) {
    try {
        const {user_id} = req.params
          User.findOne({_id:user_id}).exec(function(err, user){
            if (err) {
              console.log(err)
              var ret = {
                error: 'Something went wrong. Please try again later.'
              }
              return res.type('json').status(500).send(JSON.stringify(ret))
            } else {
                return res.type('json').status(200).send(JSON.stringify(user))
        }
      })
    } catch (e) {
      console.log(e)
      var ret = {
        error: 'Something went wrong. Please try again later.'
      }
      return res.type('json').status(500).send(JSON.stringify(ret))
    }
}

//update
exports.update = async function (req, res) {
    try {
        //check if email is already registeredQ
        var {user_id, name, email, address, password, creditcard_type,creditcard_number, creditcard_expired, creditcard_cvv } = req.body
        if(!user_id){
            var ret = {
            error: {"error":"user_id is required"}
            }
            return res.type('json').status(400).send(JSON.stringify(ret))
        }
        if(email){
        email = email.toString().toLowerCase()
          check = await User.findOne({
            email: email
          }).exec()
          if(check){
            var ret = {
              error: 'User is already registered'
            }
            return res.type('json').status(500).send(JSON.stringify(ret))
          }
        }
                var photos = []
                var files = req.files
                if(files){
                files.map((f, i) => {
                  url = f.fieldname != undefined ? ('/uploads/' + f.filename) : ''
                  photos.push(url)
                })
                }
                if(creditcard_number){
                    //credit card type validation
                    if(creditcardHelper.getCardType(creditcard_number)!=creditcard_type){
                        var ret = {
                        error: {"error":"Credit card data invalid"}
                        }
                        return res.type('json').status(400).send(JSON.stringify(ret))
                    }
                    //credit card number validation
                    if(creditcardHelper.validateCardNumber(creditcard_number)){
                        var ret = {
                          error: {"error":"Credit card data invalid"}
                        }
                        return res.type('json').status(400).send(JSON.stringify(ret))
                    }
                }
                  var dataUpdate = {
                    updatedDate: Date.now()
                  }
                  if(name) dataUpdate.name = name
                  if(email) dataUpdate.email = email
                  if(address) dataUpdate.address = address
                  if(photos) dataUpdate.photos = photos
                  if(creditcard_type) dataUpdate.creditcard_type = creditcard_type
                  if(creditcard_number) dataUpdate.creditcard_number = creditcard_number
                  if(creditcard_name) dataUpdate.creditcard_name = creditcard_name
                  if(creditcard_expired) dataUpdate.creditcard_expired = creditcard_expired
                  if(creditcard_cvv) dataUpdate.creditcard_cvv = creditcard_cvv
                  if(password)dataUpdate.password=crypto.createHash('md5').update(password).digest('hex'),
                  User.findOneAndUpdate({_id:user_id},dataUpdate, function (err, user) {
                    if (err || user == null) {
                        var ret = {
                          error: 'Something went wrong. Please try again later.'
                        }
                        return res.type('json').status(500).send(JSON.stringify(ret))
                    } else {
                        var ret = {
                            success:true
                        }
                        return res.type('json').status(200).send(JSON.stringify(ret))
                    }
            })
    } catch (e) {
      console.log(e)
      var ret = {
        error: 'Something went wrong. Please try again later.'
      }
      return res.type('json').status(500).send(JSON.stringify(ret))
    }
}