module.exports = {
    validateCardNumber: function (inputNum) {
        var digit, digits, flag, sum, _i, _len;
        flag = true;
        sum = 0;
        digits = (inputNum + '').split('').reverse();
        for (_i = 0, _len = digits.length; _i < _len; _i++) {
          digit = digits[_i];
          digit = parseInt(digit, 10);
          if ((flag = !flag)) {
            digit *= 2;
          }
          if (digit > 9) {
            digit -= 9;
          }
          sum += digit;
        }
        return sum % 10 === 0;
    },
    getCardType: function (number) {
        if(number[0]==4) return 'visa'
        if(number[0]==5) return 'mastercard'
    }
}
