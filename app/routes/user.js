const apiMiddleware = require('../middleware/api.middleware');
var multer = require('multer')
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './uploads')
    },
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + '.' + file.mimetype.split('/')[1])
    }
})
var upload = multer({
    storage: storage
})
module.exports = function (app) {
    var user = require('../controllers/user.controller')
    // Routes
    /// -------------  USER --------------- ////
    app.route('/user/register')
        .post([apiMiddleware.validateApi,upload.array('photos')],user.register) //max 5
    app.route('/user/list')
        .get(apiMiddleware.validateApi,user.list)
    app.route('/user/:user_id')
        .get(apiMiddleware.validateApi,user.get)
    app.route('/user')
        .patch(apiMiddleware.validateApi,user.update)
}