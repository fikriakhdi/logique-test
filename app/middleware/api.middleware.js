
const creditCard = require('../helpers/creditcard');
exports.validateApi = function (req, res, next){
    if(!req.header('key')){
        var ret = {
            error:'API key is missing'
        }
        return res.type('json').status(403).send(JSON.stringify(ret))
    } else {
        if(!req.header('key')==process.env.API_KEY){
            var ret = {
                error:'Invalid API key'
            }
            return res.type('json').status(401).send(JSON.stringify(ret))
        } else
        next()
    }
}