const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let UserSchema = new Schema ({
  name: String,
  address: String,
  email: String,
  password: String,
  photos: [String],
  creditcard_type: String,
  creditcard_number: String,
  creditcard_expired: String,
  creditcard_cvv: String,
  createdDate: Date,
  updatedDate: Date,
});


// Export the model
module.exports = mongoose.model('users', UserSchema)